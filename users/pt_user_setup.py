non_partner_agencies = [
    "Administration for Children's Services",
    "Art Commission",
    "Board of Correction",
    "Board of Standards and Appeals",
    "Business Integrity Commission",
    "Civil Service Commission",
    "Civilian Complaint Review Board",
    "Commission to Combat Police Corruption",
    "Conflicts of Interest Board",
    "Department for the Aging",
    "Department of Buildings",
    "Department of City Planning",
    "Department of Citywide Administrative Services",
    "Department of Consumer Affairs",
    "Department of Correction",
    "Department of Cultural Affairs",
    "Department of Design and Construction",
    "Department of Environmental Protection",
    "Department of Finance",
    "Department of Health and Mental Hygiene",
    "Department of Homeless Services",
    "Department of Housing Preservations and Development",
    "Department of Investigation",
    "Department of Parks and Recreation",
    "Department of Probation",
    "Department of Transporation",
    "Department of Youth and Community Development",
    "Design Commission",
    "Equal Employment Practices Commission",
    "Health and Hospitals Corporation",
    "Housing Recovery Operations",
    "Human Resources Administration",
    "Landmarks Preservation Commission",
    "Law Department",
    "Loft Board",
    "New York City Fire Department",
    "New York City Housing Authority",
    "Office of Environmental Remediation",
    "Office of Labor Relations",
    "Office of Long-Term Planning & Sustainability",
    "Office of Management and Budget",
    "Office of Payroll Administration",
    "Office of the Actuary",
    "Office of the Special Narcotics Prosecutor",
    "Police Department",
    "Procurement Policy Board",
    "School Construction Authority",
    "Small Business Services",
    "Taxi and Limousine Commission"
]
partner_agencies = [
    "City Commission on Human Rights",
    "Department of Education",
    "Department of Information Technology and Telecommunications",
    "Department of Records and Information Services",
    "Mayor's Office of Contract Services",
    "Mayor's Office of Media and Entertainment",
    "Office of Administrative Trials and Hearings",
    "Office of the Chief Medical Examiner",
    "Office of Emergency Management",
    "Office of the Mayor",
]

staff_users = ["", "Shanna Florio", "Willene Helfer", "Romeo Ptak", "Lashay Schwager", "Alex Paull", "Zina Laverty", "Pete Chitty", "Gayle Bonanno", "Amy Alli", "Malik Higham", "Shirlee Counce", "Nery Bozarth", "Pia Hasler", "Robbin Tews", "Phillip Killough", "Madge Bohn", "Catarina Kerwin", "Sandee Jansson", "Nova Cates", "Tabetha Caulder", "Elton Boddy", "Melvin Picou", "Season Teeters", "Bernard Bisson", "Peggy Laforge", "Marcos Sholes", "Shaunte Sandman", "Gwen Beasley", "Misha Sterrett", "Kit Stubbs", "Sandy Masek", "Shawnna Swope", "Gordon Juares", "Rudolph Gaus", "Charise Kimzey", "Roseanna Vale", "Rivka Centeno", "Ladonna Dyches", "Cyndi Briski", "Hilde Bradbury", "Mertie Manuel", "Agripina Mier", "Jenice Rigney", "Gricelda Watrous", "Cheri Boltz", "Wilson Mook", "Dawn Morrill", "Darcel Manion", "Brenna Metivier", "Marjory Snelson", "Pearline Detty", "Daisey Dilbeck", "Grazyna Mcmann", "Marla Glueck", "Zenaida Balfour", "Beverley Amendola", "Micheal Haston", "Chadwick Gimbel", "Bud Devita", "Terra Hasegawa", "Abel Ryer", "Patricia Wilton", "Jonah Salaam", "Albertina Ammann", "Vincenzo Kelso", "Elissa Pummill", "Willa Tewksbury", "Jessi Rine", "Gillian Vena", "Santina Leech", "Shera Favors", "Marjory Liebig", "Marcelo Kinley", "Ligia Gisi", "Deja Molter", "Tiana Mogan", "Tanisha Gilstrap", "Kisha Mcquiston", "Omar Comerford", "Classie Deforest", "Jeneva La", "Long Garnes", "Ted Carbajal", "Shaneka Kantor", "Clotilde Jeter", "Shawnee Frederickson", "Stacey Kist", "Nyla Ehrenberg", "Veola People", "Penelope Copeland", "Jena Bash", "Leigh Hedman", "Luana Hartford", "Loni Pariseau", "Geraldo Madill", "Kylie Hamill", "Jan Arvie", "Stanley Jacques", "Tami Auvil", "Dallas Rapozo", "Dominque Sarinana", "Daniela Tito", "Loan Hileman", "Lan Prahl", "Thomasine Nemeth", "Sudie Kesner", "Margherita Mcclellan", "Sherie Maxon", "Maryellen Degennaro", "Ellyn Schillinger", "Niki Escareno", "Gabriela Swing", "Jaleesa Akridge", "India Funkhouser", "Lynn Money", "Leia Amavisca", "Taisha Laffoon", "Maurita Ghee", "Neely Johns", "Ike Nakayama", "Everette Solomon", "Charlsie Shelly", "Beaulah Burpo", "Cyrstal Kerns", "Deedra Claro", "Klara Lindsay", "Dwight Morison", "Izetta Fort", "Reed Tellez", "Donita Lococo", "Annamarie Odom", "Lanell Solis", "Junie Parikh", "Sydney Carns", "Colleen Waren", "Efrain Espinoza", "Dalila Mazer", "Bessie Liggins", "Marlo Crayton", "Cherish Benfer", "Melida Pinkerton", "Peggy Castillo", "Soraya Clawson", "Lance Arispe", "Johnathon Calixte", "Katharyn Hinojos", "Merideth Pastrana", "Clarisa Kerwin", "George Bowne", "Cari Harrah", "Clayton Longshore", "Arvilla Mcquinn", "Adrianna Rogan", "Jama Liner", "Cherish Lehr", "Sybil Grell", "Emanuel Eudy", "Sebrina Giusti", "Denna Cervone", "Renita Donohue", "Drucilla Mcmillen", "Felipa Pratte", "Ima Funches", "Pearle Baskin", "Chance Dominguez", "Kelsey Oconner", "Serina Tarpley", "Shelba Percell", "Carylon Irvin", "Rodrick Rumfelt", "Jodee Kamm", "Bonita Waid", "Kera Bethune", "Hollis Erwin", "Cheryll Krauss", "Alyse Banvelos", "Machelle Spell", "Ramona Mcreynolds", "Vicki Celestin", "Marquerite Gallager", "Lang Selvidge", "Lauren Becerril", "Candis Shook", "Isaac Lay", "Brent Synder", "Lavonne Tanguay", "Sheryll Deslauriers", "Lakita Orlowski", "Carmella Cockrum", "Carla Fillers", "Hellen Bergen", "Casey Mcgrath", "Casimira Garney", "Wan Sotomayor", "Bruna Estepp", "Latanya Lundblad", "Norma Propes", "Tomiko Wirt", "Luba Condrey", "Ernestine Caverly", "Melinda Battaglia", "Celena Winslett", "Alfreda Hogge", "Abel Mancilla", "Maryjo Alli", "Nelly Snedeker", "Marleen Galles", "Nova Woltz", "Heide Lowrance", "Mia Lor", "Genoveva Waites", "Lasandra Kivi", "Lakeshia Engel", "Darla Lightcap", "Lavonna Borrero", "Bryanna Dyson", "Oneida Laverriere", "Christin Sleeth", "Collene Malloy", "Ann Fenske", "Sachiko Stambaugh", "Edmund Sester", "Heather Olsen", "Jerlene Sircy", "Londa Valdes", "Carleen Vanhook", "Flora Alonzo", "Elida Pratte", "Terisa Mccaul", "Wynell Marsden", "Theda Andersen", "Morton Marmolejo", "Jetta Fager", "Katy Barnhart", "Francesca Dickson", "Bobbie Molloy", "Bryon Noggle", "Broderick Vachon", "Casimira Popp", "Terrilyn Overton", "Caroll Wilcoxon", "Noble Sherer", "Hae Scheid", "Renate Fullwood", "Myra Arnot", "Roy Motes", "Nancy Swoboda", "Lakita Brigance", "Patrick Coatney", "Erick Boyd", "September Fleurant", "Johanne Geiser", "Megan Denicola", "Leandra Loflin", "Kathern States", "Doug Watanabe", "Elayne Uyehara", "Jerrica Hamp", "Derrick Jimenez", "Ivonne Alderete", "Audra Mcgrew", "Grover Kua", "Luisa Tandy", "Tad Isaac", "Von Tibbles", "Gus Byrd", "Augustina Shoultz", "Talisha Oja", "Leonard Callejas", "Vernice Steiner", "Charlette Laurin", "Antonio Cimini", "Danyell Piscitelli", "Hildegard Wain", "Numbers Meadows", "Toshia Rimmer", "Sandee Gatewood", "Perla Tighe", "Elijah Chock", "Altha Mckean", "Ryan Fee", "Chanelle Christain", "Genevive Marquardt", "Elizebeth Clauss", "Mario Balli", "Kasha Billing", "Juliette Mccaffrey", "Elaina Pendelton", "Chet Lao", "Danna Brink", "Daniel Ankrom", "Alta Belvins", "Priscilla Gaetano", "Eli Braley", "Hisako Drayton", "Christen Musil", "Edyth Magruder", "Pura Hillier", "Aide Simas", "Dagny Crisman", "Johnnie Suarez", "Matilde Cybart", "Marlo Corwin", "Darrel Mcfalls", "Lynnette Lochner", "Charisse Deaver", "Yuki Bankes", "Melodee Amyx", "Ranee Hegland", "Millie Vorce", "Dee Vanvliet", "Lorita Mogan", "Genia Choquette", "Angelita Paschall", "Tequila Ruvolo", "Carlotta Truax", "Birgit Heredia", "Chieko Frenz", "Bronwyn Lauffer", "Traci Alsop", "Alanna Plants", "Deangelo Keiper", "Dia Byham", "Talisha Turnbough", "Edwin Pineiro", "Odelia Radley", "Nenita Arteaga", "Danille Cygan", "Ray Boze", "Gilbert Lasker", "Delora Mcconico", "Rossie Minter", "Chanel Varian", "Josphine Forshee", "Shavon Cannady", "Dacia Rogowski", "Neoma Considine", "Toney Accardi", "Dusty Pursell", "Elva Hsu", "Dorthea Vanbrunt", "Cary Merino", "Brittny Goldschmidt", "Maile Crowther", "Arnold Henline", "Tammara Mosca", "Ming Stampley", "Dominic Mccaskey", "Tess Lattimore", "Lekisha Loaiza"]

k = 1
staff = open("staff_pt.csv", "w")
liaisons = open("liaisons_pt.csv", "w")

liaisons.write('department name,PRR liaison,PRR backup\n')
staff.write('name,email,department name,phone number,role\n')

for i in range(len(partner_agencies)):
    liaisons.write("%s,bladerunnerperftest%02d@dsny.nyc.gov\n" % (partner_agencies[i], k))
    for j in range(4):
        staff.write("%s,bladerunnerperftest%02d@dsny.nyc.gov,%s,311,Portal Administrator\n" %
                    (staff_users[k], k, partner_agencies[i]))
        k += 1
    for j in range(4):
        staff.write("%s,bladerunnerperftest%02d@dsny.nyc.gov,%s,311,Agency Administrator\n" %
                    (staff_users[k], k, partner_agencies[i]))
        k += 1
    for j in range(11):
        staff.write("%s,bladerunnerperftest%02d@dsny.nyc.gov,%s,311,Agency FOIL Officer\n" %
                    (staff_users[k], k, partner_agencies[i]))
        k += 1
    for j in range(11):
        staff.write("%s,bladerunnerperftest%02d@dsny.nyc.gov,%s,311,Agency Helpers\n" %
                    (staff_users[k], k, partner_agencies[i]))
        k += 1
for i in range(len(non_partner_agencies) - 1):
    staff.write("%s,bladerunnerperftest%02d@dsny.nyc.gov,%s,311,Agency Administrator\n" %
                    (staff_users[k], k, non_partner_agencies[i]))
    liaisons.write("%s,bladerunnerperftest%02d@dsny.nyc.gov\n" % (non_partner_agencies[i], k))
    k += 1

